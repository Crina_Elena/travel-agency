There are 3 files: index.html style.css script.js

-- index.html --
The page has 6 sections:
	1. About us
	2. Services
	3. Portofolio
	4. Offers
	5. Team
	6. Contact

-- style.css --
Page design.

-- script.js --
Page effects.

Framework: Bootstrap 3.0

The site is implemented as mobile responsive.